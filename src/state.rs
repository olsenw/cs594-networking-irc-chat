use std::cell::RefCell;
use std::collections::HashMap;
use std::error::Error;
use std::fmt;
use std::io;
use std::net::{Shutdown, TcpStream};
use std::rc::Rc;
use std::sync::mpsc::Sender;
use std::thread::JoinHandle;

use crate::*;

struct Client {
    addr: String,
    stream: TcpStream,
}

impl Client {
    fn new(stream: &TcpStream) -> io::Result<Client> {
        let addr = stream.peer_addr()?.to_string();
        let stream = stream.try_clone()?;
        println!("{} -> Client Connected", addr);
        Ok(Client { addr, stream })
    }

    fn get_addr(&self) -> &str {
        &self.addr
    }

    fn send_message(&mut self, msg: &Message) -> io::Result<()> {
        send_string(&mut self.stream, &msg.encode())
    }
}

impl Drop for Client {
    fn drop(&mut self) {
        if self.stream.shutdown(Shutdown::Both).is_err() {
            // ignore stream shutdown error
        }
        println!("{} -> Client Disconnected", self.addr);
    }
}

type RClient = Rc<RefCell<Client>>;

struct Room {
    name: String,
    clients: HashMap<String, RClient>,
}

impl Room {
    fn new(name: &str) -> Self {
        println!("{} -> Room Opened", name);
        Room {
            name: name.to_string(),
            clients: HashMap::new(),
        }
    }

    fn get_name(&self) -> &str {
        &self.name
    }

    fn is_empty(&self) -> bool {
        self.clients.is_empty()
    }

    fn add_client(&mut self, client: &RClient) -> Option<RClient> {
        let addr = client.borrow().get_addr().to_string();
        let rc = Rc::clone(client);
        self.clients.insert(addr, rc)
    }

    fn contains_client(&self, client: &str) -> bool {
        self.clients.contains_key(client)
    }

    fn list_client_addrs(&self) -> Vec<String> {
        self.clients
            .values()
            .map(|v| v.borrow().get_addr().to_string())
            .collect()
    }

    fn remove_client(&mut self, client: &str) -> Option<RClient> {
        self.clients.remove(client)
    }

    fn send_message(&mut self, msg: &Message) -> io::Result<()> {
        for (_addr, client) in self.clients.iter() {
            client.borrow_mut().send_message(msg)?;
        }
        Ok(())
    }
}

impl Drop for Room {
    fn drop(&mut self) {
        println!("{} -> Room Closed", self.name);
    }
}

#[derive(Debug)]
pub enum ServerStateError {
    Io(io::Error),
    Connection,
    NoSuchClient,
    ClientNotInRoom,
    NoSuchRoom,
}

impl Error for ServerStateError {}

impl fmt::Display for ServerStateError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use ServerStateError::*;
        match self {
            Io(_err) => write!(f, "Error - io error occurred"),
            Connection => write!(f, "Error failed to create connection"),
            NoSuchClient => write!(f, "Error no such client connected"),
            ClientNotInRoom => write!(f, "Error client cannot message room without joining"),
            NoSuchRoom => write!(f, "Error no such room exists"),
        }
    }
}

#[derive(Default)]
pub struct ServerState {
    clients: HashMap<String, RClient>,
    rooms: HashMap<String, Room>,
}

impl ServerState {
    pub fn new() -> Self {
        ServerState {
            clients: HashMap::new(),
            rooms: HashMap::new(),
        }
    }

    pub fn new_client(
        &mut self,
        stream: &TcpStream,
        sender: &Sender<ChannelData>,
    ) -> Result<JoinHandle<()>, ServerStateError> {
        // create client
        let client = match Client::new(&stream) {
            Ok(client) => client,
            Err(err) => return Err(ServerStateError::Io(err)),
        };
        // get addr which will use as key
        let addr = client.get_addr().to_string();
        // make sure this key is not already present
        if self.clients.contains_key(&addr) {
            return Err(ServerStateError::Connection);
        }
        // add client to server state
        self.clients.insert(addr, Rc::new(RefCell::new(client)));
        // create thread to get input from client
        match tcp_reader(stream, sender) {
            Ok(handle) => Ok(handle),
            Err(err) => Err(ServerStateError::Io(err)),
        }
    }

    pub fn remove_client(&mut self, client: &str) -> Result<(), ServerStateError> {
        // make sure client to remove is actually present on server
        if !self.clients.contains_key(client) {
            return Err(ServerStateError::NoSuchClient);
        }
        // remove client from rooms
        let mut empty = Vec::new();
        for r in self.rooms.values_mut() {
            r.remove_client(client);
            if r.is_empty() {
                empty.push(r.get_name().to_string());
            }
        }
        // delete empty rooms
        for r in empty {
            self.rooms.remove(&r);
        }
        // remove client
        self.clients.remove(client);
        Ok(())
    }

    pub fn join_room(&mut self, client: &str, room: &str) -> Result<(), ServerStateError> {
        // make sure client to remove is actually present on server
        if let Some(client) = self.clients.get(client) {
            // create room if it does not already exist
            if !self.rooms.contains_key(room) {
                self.rooms.insert(room.to_string(), Room::new(room));
            }
            // add client to room
            if let Some(room) = self.rooms.get_mut(room) {
                room.add_client(client);
            }
            let msg = format!("Joined room: {}", room);
            send_response(client, msg)
        } else {
            Err(ServerStateError::NoSuchClient)
        }
    }

    pub fn leave_room(&mut self, client: &str, room: &str) -> Result<(), ServerStateError> {
        // make sure client to remove is actually present on server
        if let Some(c) = self.clients.get_mut(client) {
            if let Some(r) = self.rooms.get_mut(room) {
                r.remove_client(client);
                if r.is_empty() {
                    self.rooms.remove(room);
                }
                let msg = format!("Left room: {}", room);
                send_response(c, msg)
            } else {
                Err(ServerStateError::NoSuchRoom)
            }
        } else {
            Err(ServerStateError::NoSuchClient)
        }
    }

    pub fn rooms_string(&self) -> String {
        self.rooms
            .keys()
            .cloned()
            .collect::<Vec<String>>()
            .join(&"\n")
    }

    pub fn list_rooms(&mut self, client: &str) -> Result<(), ServerStateError> {
        let s = self.rooms_string();
        // make sure client to remove is actually present on server
        if let Some(client) = self.clients.get_mut(client) {
            let msg = format!("Rooms present on server:\n{}", s);
            send_response(client, msg)
        } else {
            Err(ServerStateError::NoSuchClient)
        }
    }

    pub fn users_string(&self) -> String {
        self.clients
            .keys()
            .cloned()
            .collect::<Vec<String>>()
            .join(&"\n")
    }

    pub fn members_room_string(&self, room: &str) -> Result<String, ServerStateError> {
        if let Some(r) = self.rooms.get(room) {
            Ok(r.list_client_addrs().join(&"\n"))
        } else {
            Err(ServerStateError::NoSuchRoom)
        }
    }

    pub fn room_members(&mut self, client: &str, room: &str) -> Result<(), ServerStateError> {
        // make sure client to remove is actually present on server
        if let Some(client) = self.clients.get_mut(client) {
            if let Some(r) = self.rooms.get(room) {
                let msg = format!(
                    "Room: {} Members:\n{}",
                    room,
                    r.list_client_addrs().join(&"\n")
                );
                send_response(client, msg)
            } else {
                Err(ServerStateError::NoSuchRoom)
            }
        } else {
            Err(ServerStateError::NoSuchClient)
        }
    }

    pub fn message(&mut self, client: &str, room: &str, msg: &str) -> Result<(), ServerStateError> {
        // make sure client to remove is actually present on server
        if self.clients.contains_key(client) {
            if let Some(r) = self.rooms.get_mut(room) {
                if !r.contains_client(client) {
                    return Err(ServerStateError::ClientNotInRoom);
                }
                let msg = format!("Room: {} User: {}\n{}", room, client, msg);
                let msg = Message::Response { response: msg };
                if let Err(err) = r.send_message(&msg) {
                    return Err(ServerStateError::Io(err));
                }
                Ok(())
            } else {
                Err(ServerStateError::NoSuchRoom)
            }
        } else {
            Err(ServerStateError::NoSuchClient)
        }
    }

    pub fn whisper(
        &mut self,
        client: &str,
        target_client: &str,
        msg: &str,
    ) -> Result<(), ServerStateError> {
        // make sure client to remove is actually present on server
        if self.clients.contains_key(client) {
            if let Some(target) = self.clients.get_mut(target_client) {
                let msg = format!("Whisper from: {}\n{}", client, msg);
                send_response(target, msg.to_string())
            } else {
                Err(ServerStateError::NoSuchClient)
            }
        } else {
            Err(ServerStateError::NoSuchClient)
        }
    }

    pub fn invalid_message(&mut self, client: &str, msg: &str) -> Result<(), ServerStateError> {
        // make sure client to remove is actually present on server
        if let Some(client) = self.clients.get_mut(client) {
            send_response(client, msg.to_string())
        } else {
            Err(ServerStateError::NoSuchClient)
        }
    }
}

fn send_response(client: &RClient, msg: String) -> Result<(), ServerStateError> {
    let msg = Message::Response { response: msg };
    if let Err(err) = client.borrow_mut().send_message(&msg) {
        Err(ServerStateError::Io(err))
    } else {
        Ok(())
    }
}
