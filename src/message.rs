// use clap::{App, Arg, AppSettings, SubCommand};
use std::fmt;

// enum of all the message types
#[derive(Clone)]
pub enum Message {
    //User { name: String },         // update username
    Join { room: String },                       // join room
    Leave { room: String },                      // leave room
    List,                                        // list rooms on server
    Members { room: String },                    // members in room
    Response { response: String },               // response to list and members messages
    Message { room: String, message: String },   // send message to members in room
    Whisper { client: String, message: String }, // send message to client
}

impl Message {
    // take a message and turn it into a string
    pub fn encode(&self) -> String {
        // get all the messages types to save on typing
        use self::Message::*;
        // match against which type of message and make a string
        match self {
            //User { name } => format!("User {}", name),
            Join { room } => format!("Join {}", room),
            Leave { room } => format!("Leave {}", room),
            List => "List".to_string(),
            Members { room } => format!("Members {}", room),
            Response { response } => format!("Response {}", response),
            Message { room, message } => format!("Message {} {}", room, message),
            Whisper { client, message } => format!("Whisper {} {}", client, message),
        }
    }

    // attempt to take a string and turn it into a message
    pub fn decode(string: &str) -> Option<Self> {
        // get all the messages types to save on typing
        use self::Message::*;
        // split string on newlines
        let split: Vec<&str> = string.trim_end().splitn(3, ' ').collect();
        // match on first string in split
        match split[0] {
            /*"User" if split.len() == 2 => Some(User {
                name: split[1].to_string(),
            }),*/
            "Join" if split.len() == 2 => Some(Join {
                room: split[1].to_string(),
            }),
            "Leave" if split.len() == 2 => Some(Leave {
                room: split[1].to_string(),
            }),
            "List" if split.len() == 1 => Some(List),
            "Members" if split.len() == 2 => Some(Members {
                room: split[1].to_string(),
            }),
            "Response" if split.len() >= 2 => Some(Response {
                response: split[1..].join(&" "),
            }),
            "Message" if split.len() == 3 => Some(Message {
                room: split[1].to_string(),
                message: split[2].to_string(),
            }),
            "Whisper" if split.len() == 3 => Some(Whisper {
                client: split[1].to_string(),
                message: split[2].to_string(),
            }),
            _ => None,
        }
    }
}

impl fmt::Display for Message {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // get all the messages types to save on typing
        use self::Message::*;
        // match against which type of message and make a string
        match self {
            /*User { name } => write!(
                f,
                "Client wishes to change their display name to \"{}\"",
                name
            ),*/
            Join { room } => write!(f, "Client wishes to join room \"{}\"", room),
            Leave { room } => write!(f, "Client wishes to leave room \"{}\"", room),
            List => write!(f, "Requesting list of rooms on Server"),
            Members { room } => write!(f, "Requesting members for room \"{}\"", room),
            Response { response } => write!(f, "{}", response),
            Message { room, message } => write!(f, "From \"{}\"\n{}", room, message),
            Whisper { client, message } => {
                write!(f, "Private message from \"{}\"\n{}", client, message)
            }
        }
    }
}

impl fmt::Debug for Message {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // get all the messages types to save on typing
        use self::Message::*;
        // match against which type of message and make a string
        match self {
            //User { name } => write!(f, "User {}", name),
            Join { room } => write!(f, "Join {}", room),
            Leave { room } => write!(f, "Leave {}", room),
            List => write!(f, "List"),
            Members { room } => write!(f, "Members {}", room),
            Response { response } => write!(f, "Response {}", response),
            Message { room, message } => write!(f, "Message {} {}", room, message),
            Whisper { client, message } => write!(f, "Whisper {} {}", client, message),
        }
    }
}
