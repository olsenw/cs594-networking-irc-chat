mod channel_thread;
mod message;
mod state;

// allow these to be visible by callers of lib
pub use channel_thread::*;
pub use message::*;
pub use state::*;

// constants
pub const SERVER_ADDR: &str = "127.0.0.1:7878"; // default address the server is going to listen on
pub const MAX_MESSAGE_SIZE: usize = 2048;

// handy utility functions
use std::io::{self, prelude::*};
use std::net::TcpStream;
pub fn send_string(stream: &mut TcpStream, string: &str) -> io::Result<()> {
    //println!("Sending -> {}", string);
    let mut string = string.to_string();
    string.truncate(MAX_MESSAGE_SIZE); // truncate string to max message size if needed
    stream.write_all(string.as_bytes())?;
    stream.flush()?;
    Ok(())
}
