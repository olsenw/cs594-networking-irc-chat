// threads that have a communication channel

use std::io::{self, prelude::*};
use std::net::{TcpListener, TcpStream};
use std::sync::mpsc::Sender;
use std::thread::{Builder, JoinHandle};

use crate::MAX_MESSAGE_SIZE;

// enum of things that can be sent over the channel
pub enum ChannelData {
    Stdin(String),             // string from stdin thread
    Stream(TcpStream),         // new stream from tcp listen thread
    TcpString(String, String), // address and string from a tcp_read thread
    Close(String),             // stream has closed
}

// creates a thread that reads from stdin and sends input over channel
pub fn stdin_reader(sender: Sender<ChannelData>) -> io::Result<JoinHandle<()>> {
    // create thread
    spawn_thread("thread_stdin", move || {
        // get handle to stdin and get user input
        for line in io::stdin().lock().lines() {
            // will panic if stdin or sender experience error
            sender.send(ChannelData::Stdin(line.unwrap())).unwrap();
        }
    })
}

// create thread to accept and then send tcp streams over channel
pub fn tcp_listener(addr: &str, sender: Sender<ChannelData>) -> io::Result<JoinHandle<()>> {
    // listen at given address
    let listener = TcpListener::bind(addr)?;
    // create thread
    spawn_thread("thread_listen", move || {
        // iterate over incoming connections
        for stream in listener.incoming() {
            // validate successful connection
            match stream {
                // client successfully connected
                Ok(stream) => sender.send(ChannelData::Stream(stream)).unwrap(),
                // error occurred on connecting client
                Err(_) => continue,
            }
        }
    })
}

// create thread to read from tcp socket and send data over channel
pub fn tcp_reader(stream: &TcpStream, sender: &Sender<ChannelData>) -> io::Result<JoinHandle<()>> {
    // create own copy of stream
    let mut stream = stream.try_clone()?;
    // create own copy of sender half of channel
    let sender = sender.clone();
    // get addr of peer (as a string)
    let peer = stream.peer_addr()?.to_string();
    // create name for thread
    let name = format!("Thread_reader({})", peer);
    // create thread
    spawn_thread(&name, move || {
        // keep reading till error (ie stream or sender close)
        loop {
            // buffer to hold input from tcp
            let mut buff = [0; MAX_MESSAGE_SIZE];
            // read input from tcp
            let num_bytes = match stream.read(&mut buff) {
                Ok(bytes) => bytes,
                Err(_) => break, // error occurred reading from socket
            };
            if num_bytes == 0 {
                // read zero bytes tcp probably closed
                break;
            }
            // send input to main thread
            let string = String::from_utf8_lossy(&buff[..num_bytes]).into_owned();
            sender
                .send(ChannelData::TcpString(peer.clone(), string))
                .unwrap();
        }
        sender.send(ChannelData::Close(peer)).unwrap();
    })
}

// create thread to take data from channel and send it over tcp socket
// pub fn tcp_writer(sender: Sender<ChannelData>) -> io::Result<JoinHandle<()>> {}

// creates a thread with given name running given closure
// returns success/fail if success also returns the join handle for thread
fn spawn_thread<F, T>(name: &str, action: F) -> io::Result<JoinHandle<T>>
where
    F: FnOnce() -> T,
    F: Send + 'static,
    T: Send + 'static,
{
    //println!("Spawning thread -> {}", name);
    Builder::new().name(name.into()).spawn(action)
}
