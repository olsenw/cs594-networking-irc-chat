use std::env;
use std::net::TcpStream;
use std::process::exit;
use std::sync::mpsc::channel;

use irc_project::*;

fn handle_message(_addr: String, string: String) {
    //println!("{} -> {}", addr, string);
    if let Some(msg) = Message::decode(&string) {
        println!("{}", msg);
    } else {
        println!("Received an invalid message from server... exiting");
        exit(1);
    }
}

fn main() {
    // create channel for communication
    let (sender, receiver) = channel();

    // create thread to read from stdin
    stdin_reader(sender.clone()).unwrap();

    // get address of server to try connecting to
    let mut addr = SERVER_ADDR.to_string();
    let args = env::args().collect::<Vec<String>>();
    if args.len() >= 2 {
        addr = args[1].clone();
    }

    // open connection to server
    let mut stream = match TcpStream::connect(&addr) {
        Ok(stream) => stream,
        Err(_) => {
            eprintln!("Failed to connect to {}", addr);
            eprintln!(
                "Are you sure you entered an ip address like this: {}",
                SERVER_ADDR
            );
            exit(1);
        }
    };
    println!("--------------------------------------------");
    println!("Connected to {}", addr);
    println!("Type \"Help\" for a list of client commands.");
    println!("--------------------------------------------");

    // create thread to read from tcp stream
    tcp_reader(&stream, &sender).unwrap();

    receiver.iter().for_each(|data| match data {
        ChannelData::Stdin(line) => {
            if line.starts_with("Quit") {
                exit(0);
            } else if line.starts_with("Help") {
                println!("\"Quit\"         - stops the client");
                println!("\"Join room\"    - tell server to join specified room");
                println!("\"Leave room\"   - tell server to leave specified room");
                println!("\"List\"         - tell server to list all available rooms");
                println!("\"Members room\" - tell server to list all clients in specified room");
                println!("\"Message room message\" - tell server to message specified room");
                println!("\"Whisper client message\" tell server to whisper to specified client");
                println!("\"Help\"         - prints help information you are reading");
            } else {
                send_string(&mut stream, &line).unwrap();
            }
        }
        ChannelData::TcpString(addr, string) => handle_message(addr, string),
        ChannelData::Close(_) => {
            eprintln!("Lost connection to Server... exiting");
            exit(1);
        }
        _ => eprintln!("Unexpected ChannelData received"),
    });
}
