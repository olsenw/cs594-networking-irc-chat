use std::env;
use std::net::TcpStream;
use std::process::exit;
use std::sync::mpsc::channel;
use std::sync::mpsc::Sender;

use irc_project::*;

fn new_stream(state: &mut ServerState, stream: TcpStream, sender: &Sender<ChannelData>) {
    if state.new_client(&stream, sender).is_err() {
        eprintln!("Unable to accept new client... Continuing");
    }
}

fn close_stream(state: &mut ServerState, addr: &str) {
    match state.remove_client(&addr) {
        Ok(_) => {}
        Err(ServerStateError::NoSuchClient) => {}
        _ => panic!("Unexpected error closing stream"),
    }
}

fn handle_stdin(state: &mut ServerState, string: String) {
    let split: Vec<&str> = string.trim_end().splitn(3, ' ').collect();
    match split[0] {
        "Quit" => exit(0),
        "Disconnect" if split.len() == 2 => close_stream(state, split[1]),
        "Rooms" if split.len() == 1 => println!("{}", state.rooms_string()),
        "Connected" if split.len() == 1 => println!("{}", state.users_string()),
        "Members" if split.len() == 2 => match state.members_room_string(split[1]) {
            Ok(members) => println!("{}", members),
            Err(ServerStateError::NoSuchRoom) => eprintln!("No such room"),
            _ => panic!("unexpected error stdin members command"),
        },
        "Help" if split.len() == 1 => {
            println!("\"Quit\"               - stops the server");
            println!("\"Disconnect address\" - disconnects client at address");
            println!("\"Rooms\"              - lists all the rooms on the server");
            println!("\"Connected\"          - lists all the connected client's");
            println!("\"Members room\"       - lists all the members of a room");
            println!("\"Help\"               - prints help information you are reading");
        }
        _ => eprintln!("Unknown or Mistyped Command... Try \"Help\" For Command List"),
    }
}

fn handle_message(state: &mut ServerState, addr: String, string: String) {
    // decode and process message
    if let Some(msg) = Message::decode(&string) {
        //println!("From \"{}\" -> {:?}", addr, msg);
        let result = match msg {
            Message::Join { room } => state.join_room(&addr, &room),
            Message::Leave { room } => state.leave_room(&addr, &room),
            Message::List => state.list_rooms(&addr),
            Message::Members { room } => state.room_members(&addr, &room),
            Message::Message { room, message } => state.message(&addr, &room, &message),
            Message::Whisper { client, message } => state.whisper(&addr, &client, &message),
            _ => state.invalid_message(&addr, "Invalid Message"),
        };
        match result {
            Ok(_) => {}
            Err(ServerStateError::NoSuchRoom) => {
                if state
                    .invalid_message(&addr, "No Such Room Exists on Server")
                    .is_err()
                {
                    eprintln!("Error occurred sending no such room message...");
                }
            }
            Err(ServerStateError::NoSuchClient) => {
                if state
                    .invalid_message(&addr, "No Such Client Connected to Server")
                    .is_err()
                {
                    eprintln!("Error occurred sending no such client message...");
                }
            }
            Err(ServerStateError::ClientNotInRoom) => {
                if state
                    .invalid_message(&addr, "Need to Join Room Before Messaging it")
                    .is_err()
                {
                    eprintln!("Error occurred sending no such room message...");
                }
            }
            Err(err) => eprintln!("Error occurred {}", err),
        }
    } else if state.invalid_message(&addr, "Invalid Message").is_err() {
        eprintln!("Error occurred sending Invalid message...");
    }
}

fn main() {
    // create channel for communication
    let (sender, receiver) = channel();

    // create thread to read from stdin
    stdin_reader(sender.clone()).unwrap();

    // create server state
    let mut state = ServerState::new();

    // get address of server to try connecting to
    let mut addr = SERVER_ADDR.to_string();
    let args = env::args().collect::<Vec<String>>();
    if args.len() >= 2 {
        addr = args[1].clone();
    }

    // listen for connections
    if tcp_listener(&addr, sender.clone()).is_err() {
        eprintln!("Failed to tcp listener thread on {}", addr);
        eprintln!(
            "Are you sure you entered an ip address like this: {}",
            SERVER_ADDR
        );
        eprintln!("Or possibly the port you tried is already in use!");
        exit(1);
    }
    println!("--------------------------------------------");
    println!("Listening on {}", addr);
    println!("Type \"Help\" for a list of server commands.");
    println!("--------------------------------------------");

    receiver.iter().for_each(|data| match data {
        ChannelData::Stdin(line) => handle_stdin(&mut state, line),
        ChannelData::Stream(stream) => new_stream(&mut state, stream, &sender),
        ChannelData::TcpString(addr, string) => handle_message(&mut state, addr, string),
        ChannelData::Close(addr) => close_stream(&mut state, &addr),
    });
}
