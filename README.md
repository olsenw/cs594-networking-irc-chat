# Basic Internet Relay Chat (IRC)

This is a basic implementation of an IRC protocol done for CS594 Internetworking Protocols course at Portland State University.

[The protocol is defined in this RFC.](https://gitlab.com/olsenw/cs594-networking-irc-chat/blob/master/RFC.txt)

## Requirements

This project is written in the [rust programming language](https://www.rust-lang.org/) and requires the rust toolchain to be installed in order to be build and run.

## Server

To run the server, be located in the source directory and run the following command:

`Cargo run --bin server -- 127.0.0.1:7878`

You can change the address the server is listening on by changing the address argument in the above command.


While the server is running you can enter the following commands via the terminal:

* `Quit` stops the server
* `Disconnect address` disconnects client at address
* `Rooms` lists all the rooms on the server
* `Connected` lists all the connected client's
* `Members room` lists all the members of a room
* `Help` print help information

## Client

To run the client, be located in the source directory and run the following command:

`Cargo run --bin client -- 127.0.0.1:7878`

You can change the address the client is connects to by changing the address argument in the above command.

```
Currently I have a server running in the cloud at address:

34.82.92.172:7878

Which remain up as long as server does not panic and my google cloud credits hold up.
```


While the client is running you can enter the following commands via the terminal:

* `Quit` stops the client
* `Join room` tell server to join specified room
* `Leave room` tell server to leave specified room
* `List` tell server to list all available rooms
* `Members room` tell server to list all clients in specified room
* `Message room` tell server to message specified room
* `Whisper client` tell server to whisper to specified client
* `Help` print help information